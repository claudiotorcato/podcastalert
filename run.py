import os
import sys
import logging
import urllib.parse
import requests

import feedparser
from bs4 import BeautifulSoup as bs4
import atoma

import discord
from discord.ext import commands


logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

try:
    os.environ['TOKEN_PODCASTALERT']
except KeyError:
    print('Por favor, crie a variável TOKEN_PODCASTALERT')
    sys.exit(1)

TOKEN = os.environ['TOKEN_PODCASTALERT']


client = commands.Bot(command_prefix='$')
features="html.parser"
def findfeed(site):
    raw = requests.get(site).text
    result = []
    possible_feeds = []
    html = bs4(raw)#,features="xml.parser")
    feed_urls = html.findAll("link", rel="alternate")
    for f in feed_urls:
        t = f.get("type",None)
        if t:
            if "rss" in t or "xml" in t:
                href = f.get("href",None)
                if href:
                    possible_feeds.append(href)
    parsed_url = urllib.parse.urlparse(site)
    base = parsed_url.scheme+"://"+parsed_url.hostname
    atags = html.findAll("a")
    for a in atags:
        href = a.get("href",None)
        if href:
            if "xml" in href or "rss" in href or "feed" in href:
                possible_feeds.append(base+href)
    for url in list(set(possible_feeds)):
        f = feedparser.parse(url)
        if len(f.entries) > 0:
            if url not in result:
                result.append(url)
    return(result)

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.command()
async def hello(ctx, arg):
    await ctx.send(arg)

@client.command()
async def pull(ctx, url_page):
    urls = findfeed(url_page)
    await ctx.send(urls[0] + ' adicionado na base de dado')
#    response = requests.get(urls[0])
#    feed = atoma.parse_rss_bytes(response.content)
#    for post in feed.items:
#        print(post)

client.run(TOKEN)